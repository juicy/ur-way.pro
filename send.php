<?php

  require 'class.phpmailer.php';

  $types = array(
    0 => 'ooo', 1 => 'ip', 2 => 'mfo', 3 => 'oao',
    5 => 'ustav', 6 => 'dir', 7 => 'naim', 8 => 'uch',
    9 => 'deyat', 10 => 'saddr'
  );

  $name = $_POST['name'];
  $phone = $_POST['phone'];
  $email = isset($_POST['email']) ? $_POST['email'] : '';
  $comment = isset($_POST['comment']) ? $_POST['comment'] : '';
  $text = "Имя: $name \nТелефон: $phone \nEmail: $email\nКомментарий: $comment";

  if (isset($_POST['std_id'])) {
    $theme = $_POST['std_id'] < 4 ? 'Регистрация фирмы' : 'Внесение изменений';
    if (isset($_POST[$types[$_POST['std_id']]])) {
      $text .= "\n\nЧекбоксы:\n";
      $checks = $_POST[$types[$_POST['std_id']]];
      foreach ($checks as $key => $value) {
        $text .= "$key\n";
      }
    }
  }

  if (isset($_POST['buh'])) {
    $theme = 'Бухгалтерские услуги';
    $text .= "\n\n";
    $text .= 'Система налогообложения: ' . $_POST['sustem'] . "\n";
    $text .= 'Количество документов в квартал: ' . $_POST['count_documents'] . "\n";
    $text .= 'Количество сотрудников в месяц: ' . $_POST['count_peoples'] . "\n";
  }

  if (isset($_POST['ura'])) {
    include 'kit/addrs.php';
    $theme = 'Покупка юридического адреса';
    if (isset($_POST['sel'])) {
      $text .= "\n\n";
      $checks = $_POST['sel'];
      foreach ($checks as $key => $value) {
        $el = $data[intval($key)];
        $text .= $el['ifns'] . ' - ' . $el['where'] . "\n";
      }
    }
  }

  if (isset($_POST['pstamps'])) {
    $theme = 'Печати и штампы';
    if (isset($_POST['stamps'])) {
      $text .= "\n\nПечати:\n";
      foreach ($_POST['stamps'] as $key => $value) {
        $text .= "$key\n";
      }
    }
  }

  if (isset($_POST['copying'])) {
    $theme = 'Выписка из ЕГРЮЛ';
    $text .= "\n\n";
    $text .= 'Название компании: ' . $_POST['company'] . "\n";
    $text .= 'ИНН или ОГРН: ' . $_POST['inn'] . "\n";
    if (isset($_POST['ekz'])) {
      $text .= 'Экземпляров: ' . $_POST['ekz'];
    } else {
      $theme .= ' (электронная)';
    }
  }

  if (isset($_POST['sro'])) {
    $theme = 'Допуск с СРО';
    $text .= "\n\n";
    $text .= "Тип: " . $_POST["type"];
  }

  if (isset($_POST['firms'])) {
    include 'kit/firms.php';
    $theme = 'Готовые фирмы';
    if (isset($_POST['sel'])) {
      $text .= "\n\n";
      $checks = $_POST['sel'];
      foreach ($checks as $key => $value) {
        $el = $data[(int)$key];
        $text .= $el['title'] . ' - ' . $el['reg'] . ' - ' . $el['price'] . "\n";
      }
    }
  }

  if (isset($_POST['back_phone'])) {
    $theme = 'Заказ обратного звонка';
    $text = "Имя: $name \nТелефон: $phone";
  }

  $theme = 'UR-WAY.PRO: ' . $theme;

  if ($_SERVER["SERVER_NAME"] == 'ur') {
    echo $theme;
    echo "\n";
    echo $text;
    die();
  }

  $mail = new PHPMailer();
  $mail->CharSet = 'UTF-8';
  $mail->From = 'no-reply@ur-way.pro';
  $mail->FromName = 'ur-way.pro';
  $mail->AddAddress('rs@advpositive.ru', 'info@ur-way.pro');
  $mail->IsHTML(false);
  $mail->Subject = $theme;
  $mail->Body = $text;

  if (!$mail->Send()) {
    die ('Mailer Error: '.$mail->ErrorInfo);
  } else {
    echo 'ok';
  }

?>