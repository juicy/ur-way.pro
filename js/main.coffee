$(document).ready(() ->
  hidden_boxs('.hd')
  map_boxs('.map_block')
  overlay_forms('.overlay', '.open_overlay', 'a.close')
  sro_height('.sro')
  #debug_form_submit('.debug_form_submit')
  buh_page('#buh_page')
  tabs_box()
  forms('.debug_form_submit')
  tr_select('.tr_select')
  sro_title('.sro')
  dl_click('.info_table dl')
  #menu('header .menu')
)

menu = (menu) ->
  $("#{menu} ul li").click ->
    alert 1
    location.href = $(this).find('a').attr('href')
    false
  

dl_click = (dls) ->
  $(dls).each ->
    dl = this
    check = $(dl).find('input[type="checkbox"]')
    if $(check).length > 0
      $(dl).addClass('cp')
      $(dl).click ->
        $(check).click()
      $(check).click (e) ->
        e.stopPropagation()

sro_title = (box) ->
  $(box).find('.col .title input[type="radio"]')
  .click (e) ->
    e.stopPropagation()
  .parent().addClass('cp').click ->
    $(this).find('input[type="radio"]').click()

tr_select = (trs) ->
  $(trs).each ->
    tr = this
    $(tr).find('td:not(:last-child)').click -> 
      $(tr).find('input[type="checkbox"]').click()

forms = (boxs) ->
  $(boxs).each -> 
    form = this
    $(form).validate({
      rules: {
        name: "required",
        phone: "required",
        email: {
          email: true
        }
      },
      submitHandler: (form) ->
        fun_sub(form)
    });
    fun_sub = (form) ->

      # check for nil selected tr in table
      checks = $(form).find('input[name^="sel"]')
      checked_checks = $(form).find('input[name^="sel"]:checked')
      if $(checks).length > 0 && $(checked_checks).length < 1
        $('.open_overlay[oid="#overlay_not_select_anythink"]').click()
        return false

      # check for buh nil selected
      stamps = $(form).find('input[name^="stamps"]')
      checked_stamps = $(form).find('input[name^="stamps"]:checked')
      if $(stamps).length > 0 && $(checked_stamps).length < 1
        $('.open_overlay[oid="#overlay_not_select_anythink"]').click()
        return false

      # post
      $.post $(form).attr('action'), $(form).serialize(), (data, textStatus, jqXHR) ->
        if data == 'ok'
          $(form)[0].reset();
          $('.overlay:not(.hiden)').addClass('hiden')
          $('.open_overlay[oid="#overlay_success"]').click()
        else
          alert 'any tech error'
          console.log data

buh_page = (page) ->
  if $(page).length > 0

    as = $('.btns a')
    inp = $('.btns input')
    $(as).click ->
      $(as).removeClass('active')
      $(this).addClass('active')
      $(inp).val($(this).find('span').html())
      false

    $(".slider_documents").slider({
      range: "min", value: 200, min: 100, max: 1100,
      slide: (event, ui) ->
        $(".count_documents").html(ui.value)
        $('input[name="count_documents"]').val(ui.value)
    })
    $(".count_documents").html($(".slider_documents").slider("value"))
    $('input[name="count_documents"]').val($(".slider_documents").slider("value"))

    $(".slider_peoples").slider({
      range: "min", value: 10, min: 0, max: 100,
      slide: (event, ui) ->
        $(".count_peoples").html(ui.value)
        $('input[name="count_peoples"]').val(ui.value)
    })
    $(".count_peoples").html($(".slider_peoples").slider("value"))
    $('input[name="count_peoples"]').val($(".slider_peoples").slider("value"))
    

debug_form_submit = (forms) ->
  $(forms).each ->
    form = this
    $(form).submit ->
      $.post $(this).attr('action'), $(this).serialize(), (data, textStatus, jqXHR) ->
        alert data
      false

tabs_box = () ->
  $('.tab_content').first().addClass('active')
  $('section.main ul.tabs a').click ->
    id = $(this).attr('tab_id')
    $('section.main ul.tabs a').removeClass('active')
    $('.tab_content').removeClass('active')
    $('.tab_content[tab_id="'+id+'"]').addClass('active')
    $(this).addClass('active')
    false

sro_height = (box) ->
  count_lines = $(box).find('.col').first().find('> div').length
  i = 0
  while i < count_lines
    max = 0
    $(box).find('.col').each ->
      linebox = $(this).find('> div:eq(' + i + ')')
      max = $(linebox).height() if $(linebox).height() > max
    $(box).find('.col').each ->
      linebox = $(this).find('> div:eq(' + i + ')')
      $(linebox).css('height', max)
    i++

overlay_forms = (boxs, open_links, close_link) ->
  move_form = (box) ->
    form = $(box).find('.form')
    $(form).css('margin-left', -1 * ($(form).width() + 100) / 2)
    $(form).css('margin-top', -1 * ($(form).height() + 40) / 2)
  $(boxs).find(close_link).click ->
    $(this).parents('.overlay').addClass('hiden')
    false
  $(open_links).each ->
    $(this).click ->
      oid = $(this).attr('oid')
      $(oid).removeClass('hiden')
      move_form(oid)
      false

hidden_boxs = (boxs) ->
  $(boxs).each ->
    box = this
    $(box).find('a.hidden_show').click ->
      $(box).find('.hidden_text').removeClass('hide')
      false
    $(box).find('a.hidden_hide').click ->
      $(box).find('.hidden_text').addClass('hide')
      false

map_boxs = (boxs) ->
  $(boxs).each ->
    box = this
    $(box).find('a.str').click ->
      if $(box).find('.wrap').hasClass('open')
        $(box).find('.wrap').animate({
          height: 40
        }, 500)
        $(box).find('.wrap').removeClass('open')
      else
        $(box).find('.wrap').animate({
          height: 530
        }, 500)
        $(box).find('.wrap').addClass('open')
      false