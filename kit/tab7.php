<?php //смена учредит
  $text = array(
    'id' => 8,
    'step1' => '
      <h4>Заявка на сайте</h4>
      <p>Вы получите бесплатную консультацию юриста.</p>
           ',
    'step2' => '
    <h4>Подписывание документов</h4>
      <p>Подписанные документы заверяются у нотариуса и подаются в налоговую</p>
        ',
   'step3' => "
      <h4>Получение документов</h4>
          ",
    'after_steps' => "
          <h3>Смена Учредителей</h3>
        <div class='info_table center'>
    <dl>
      <dt>
        <input type='checkbox' name='uch[\"Смена Учредителя через договор купли-продажи\"]'>
        Смена Учредителя через договор купли-продажи
      </dt>
      <dd>
        <strong>15 000</strong>
        руб.
      </dd>
    </dl>
    <dl>
      <dt>
        <input type='checkbox' name='uch[\"Смена Учредителя через увеличение Уставного капитала\"]'>
        Смена Учредителя через увеличение Уставного капитала
      </dt>
      <dd>
        <strong>20 000</strong>
        руб.
      </dd>
    </dl>
     <dl>
      <dt>
        <input type='checkbox' name='uch[\"Ввод нового Учредителя\"]'>
        Ввод нового Учредителя
      </dt>
      <dd>
        <strong>12 000</strong>
        руб.
      </dd>
    </dl>
     <dl>
      <dt>
        <input type='checkbox' name='uch[\"Вывод Учредителя\"]'>
        Вывод Учредителя
      </dt>
      <dd>
        <strong>8 000</strong>
        руб.
      </dd>
    </dl>
  </div></p>
    ",
    'left' => "
      <div class='info_table'>
        <h5>Дополнительные расходы:</h5>
        <dl>
          <dt>Гос пошлина (Устав)</dt>
          <dd><strong>800</strong> руб.</dd>
        </dl>
         <dl>
          <dt>нотариальные расходы (договор купли-продажи)</dt>
          <dd><strong>15000</strong> руб.</dd>
        </dl>
        <dl>
        <dl>
          <dt>нотариальные расходы</dt>
          <dd><strong>1000–1500</strong> руб.</dd>
        </dl>
        <dl>
          <dt>нотариальные расходы (электронная подача документов)</dt>
          <dd><strong>2500-3500</strong> руб.</dd>
        </dl>
        <h5>Услуги по желанию клиента:</h5>
        <p>Отметьте галочкой интересующие Вас услуги</p>
        <dl>
          <dt><input type='checkbox' name='uch[\"Выписка из ЕГРЮЛ\"]'> Выписка из ЕГРЮЛ</dt>
          <dd><strong>1500</strong> руб.</dd>
        </dl>
        <dl>
          <dt><input type='checkbox' name='uch[\"Проверка сотрудников на судимость\"]'> Проверка сотрудников на судимость</dt>
          <dd><strong>3000</strong> руб.</dd>
        </dl>
         <dl>
          <dt><input type='checkbox' name='uch[\"Открытие расчетного счета в Банке\"]'> Открытие расчетного счета в Банке</dt>
          <dd><strong>5000-10000</strong> руб.</dd>
        </dl>
          </div>
    ",
    'info_block' => "
      <h5>От заказчика потребуются:</h5>
      <p>- свидетельство о государственной регистрации</p>
      <p>- свидетельство о постановке на учёт</p>
      <p>- устав (последняя редакция)</p>
      <p>- выписка из ЕГРЮЛ (не старше 30 дней)</p>
      <p>- решение / протокол (последний)</p>
      <p>- копии паспортов новых Участников</p>
      <p>- номера ИНН всех Участников и Ген.директора</p>
    "
  );
?>