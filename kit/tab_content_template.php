<form action="send.php" method="post" class="debug_form_submit">
  <input type="hidden" name="std_id" value="<?php echo $text['id'] ?>" />
  <div class="tab_content" tab_id="<?php echo $text['id'] ?>">
    <div class='steps'>
      <div class='step'>
        <div class='title'>шаг 1</div>
        <img alt='' src='i/im1.png'>
        <?php echo $text['step1'] ?>
      </div>
      <div class='str_step'></div>
      <div class='step'>
        <div class='title'>шаг 2</div>
        <img alt='' src='i/im2.png'>
        <?php echo $text['step2'] ?>
      </div>
      <div class='str_step'></div>
      <div class='step'>
        <div class='title'>шаг 3</div>
        <img alt='' src='i/im3.png'>
        <?php echo $text['step3'] ?>
      </div>
      <div class='clear'></div>
    </div>
    
    <?php echo $text['after_steps'] ?>
    
    <div class='send_line'>
      <a href=''>
        <img class='open_overlay' oid='#overlay<?php echo $text['id'] ?>' src='i/button1.png'>
      </a>
    </div>
    <div class='index_data'>
      <div class='left'>
        <?php echo $text['left'] ?>
      </div>
      <div class='right'>
        <div class='info_block'>
          <div class='str'></div>
          <?php echo $text['info_block'] ?>
        </div>
      </div>
      <div class='clear'></div>
    </div>
    <div class='send_line without_bg'>
      <a href=''>
        <img class='open_overlay' oid='#overlay<?php echo $text['id'] ?>' src='i/button1.png'>
      </a>
    </div>
  </div>
  <div class='overlay hiden' id='overlay<?php echo $text['id'] ?>'>
    <div class='form std_form'>
      <a class='close' href=''></a>
      <h4>Заявка</h4>
      <h5>на регистрацию фирмы</h5>
      <div class='field'>
        <input placeholder='Ваше имя *' name="name" type='text'>
      </div>
      <div class='field'>
        <input placeholder='Телефон *' name="phone" type='text'>
      </div>
      <div class='field'>
        <input placeholder='E-mail' name="email" type='text'>
      </div>
      <div class='field'>
        <textarea placeholder='Комментарий' name="comment"></textarea>
      </div>
      <p>* Обязательно к заполнению</p>
      <div class='field'>
        <input src='i/button1.png' type='image'>
      </div>
    </div>
  </div>
</form>