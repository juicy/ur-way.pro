<?php
  $text = array(
    'id' => 0,
    'step1' => '
      <h4>Заявка на сайте</h4>
      <p>Вы получите бесплатную консультацию юриста.</p>
      <p>У меня нет юр. адреса, заказать.</p>
    ',
    'step2' => '
      <h4>Подписывание документов и заверение у нотариуса.</h4>
      <p>(Мы сами отвезём в налоговую по Вашей доверенности).</p>
      <p>При себе иметь ПАСПОРТ.</p>
    ',
    'step3' => "
      <h4>Получение документов</h4>
      <p>в комплекте с печатью, кодами статистики и документами для открытия расчетного счёта</p>
      <div class='hd'>
        <p class='green'>
          <a class='hidden_show' href=''>подробнее</a>
          &darr;
        </p>
        <div class='hidden_text hide'>
          <h4>После регистрации вы получите:</h4>
          <p>• свидетельство о государственной регистрации</p>
          <p>• свидетельство о постановке на учет</p>
          <p>• устав</p>
          <p>• выписка из ЕГРЮЛ решение / протокол о создании</p>
          <p>• приказ о назначении Генерального директора</p>
          <p>• приказ о назначении Главного бухгалтера</p>
          <p>• акт оценки имущества, вносимого в уставный капитал</p>
          <p>• акт приема-передачи имущества на баланс</p>
          <p>• информационное письмо кодов статистики</p>
          <p>• список участников</p>
          <p>• договор аренды (при покупке юридического адреса)</p>
          <p>• печать на обычной оснастке</p>
          <p class='green'>
            <a class='hidden_hide' href=''>свернуть</a>
          </p>
        </div>
      </div>
    ",
    'after_steps' => "
      <h3>Регистрация ООО<br>всего за<big>6999</big>рублей!</h3>
      <h4 class='main'>Оставьте заявку сейчас и уже через <strong>10 дней</strong> ваша компания будет зарегистрирована!</h4>
    ",
    'left' => "
      <div class='info_table'>
        <h5>Дополнительные расходы:</h5>
        <dl>
          <dt>гос. пошлина</dt>
          <dd><strong>4 000</strong> руб.</dd>
        </dl>
        <dl>
          <dt>нотариальные расходы</dt>
          <dd><strong>800–1200</strong> руб.</dd>
        </dl>
        <dl>
          <dt>нотариальные расходы (электронная подача документов)</dt>
          <dd><strong>2300–3300</strong> руб.</dd>
        </dl>
        <h5>Услуги по желанию клиента:</h5>
        <p>Отметьте галочкой интересующие Вас услуги</p>
        <dl>
          <dt><input type='checkbox'> Печать на автоматической оснастке</dt>
          <dd><strong>900</strong> руб.</dd>
        </dl>
        <dl>
          <dt><input type='checkbox'> Внесение в реестр субъектов малого предпринимательства</dt>
          <dd><strong>1500</strong> руб.</dd>
        </dl>
        <dl>
          <dt><input type='checkbox'>Юридические адреса</dt>
          <dd><strong>9000</strong> руб.</dd>
        </dl>
        <dl>
          <dt><input type='checkbox'> Почтово-секретарское обслуживание</dt>
          <dd><strong>600</strong> руб.</dd>
        </dl>
      </div>
    ",
    'info_block' => "
      <h5>Преимущества ООО:</h5>
      <p>• Относительно просто зарегистрировать и ликвидировать</p>
      <p>• Просто внести изменения или продать долю</p>
      <p>• Минимальный размер уставного капитала всего 10 000 рублей</p>
      <p>• Учредители ООО не отвечают по его обязательствам и несут риск в пределах своей доли уставного капитала</p>
    "
  );
?>
<div class="tab_content" tab_id="<?php echo $text['id'] ?>">
  <div class='steps'>
    <div class='step'>
      <div class='title'>шаг 1</div>
      <img alt='' src='i/im1.png'>
      <?php echo $text['step1'] ?>
    </div>
    <div class='str_step'></div>
    <div class='step'>
      <div class='title'>шаг 2</div>
      <img alt='' src='i/im2.png'>
      <?php echo $text['step2'] ?>
    </div>
    <div class='str_step'></div>
    <div class='step'>
      <div class='title'>шаг 3</div>
      <img alt='' src='i/im3.png'>
      <?php echo $text['step3'] ?>
    </div>
    <div class='clear'></div>
  </div>
  
  <?php echo $text['after_steps'] ?>
  
  <div class='send_line'>
    <a href=''>
      <img class='open_overlay' oid='#overlay1' src='i/button1.png'>
    </a>
  </div>
  <div class='index_data'>
    <div class='left'>
      <?php echo $text['left'] ?>
    </div>
    <div class='right'>
      <div class='info_block'>
        <div class='str'></div>
        <?php echo $text['info_block'] ?>
      </div>
    </div>
    <div class='clear'></div>
  </div>
  <div class='send_line without_bg'>
    <a href=''>
      <img class='open_overlay' oid='#overlay1' src='i/button1.png'>
    </a>
  </div>
</div>